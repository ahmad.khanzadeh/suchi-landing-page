// show menu 
const showMenu = (toggleId, navId) =>{
    const toggle = document.getElementById(toggleId);
    const nav =document.getElementById(navId);

    if(toggle && nav){
        toggle.addEventListener('click',()=>{
            nav.classList.toggle('show-menu');
            
        })
    }
}

showMenu('nav-toggle' , 'nav-menu');

//  remove Menu Mobile

const navLink = document.querySelectorAll('.nav__link')

function linkAction(){
    const navMenu = document.getElementById('nav-menu')
    navMenu.classList.remove('show-menu')
}
navLink.forEach(n => n.addEventListener('click', linkAction))

// scroll section active link

const sections= document.querySelectorAll('section[id]')

window.addEventListener('scroll', scrollActive)

function scrollActive(){
    const scrollY = window.pageYOffset

    sections.forEach( current =>{
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop -50,
        sectionId = current.getAttribute('id')

        if(scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.add('active-link')
        }else{
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.remove('active-link')
        }
    })
}

window.addEventListener('scroll', scrollActive)

// change background header

function scrollHeader(){
    const nav = document.getElementById('header')

    if(this.scrollY >=200) nav.classList.add('scroll-header'); else nav.classList.remove('scroll-header') 
}

window.addEventListener('scroll', scrollHeader)

// show scroll top
function scrollTop(){
    const scrollTop=document.getElementById('scroll-top')
    if(this.scrollY >= 560) scrollTop.classList.add('scroll-top'); else scrollTop.classList.remove('scroll-top')
}

window.addEventListener('scroll' , scrollHeader)

// =============================dark and light theme
const themeButton = document.getElementById('theme-button')
const darktheme='dark-theme'
const iconTheme='bx-sun'

//=============Previously selected topic (if user selected)
const selectedTheme = localStorage.getItem('selected-theme')
const selectedIcon = localStorage.getItem('selected-icon')

//optain current theme 
const getCurrentTheme= () => document.body.classList.contains(darktheme) ? 'dark' : 'light'
const getCurrentIcon=() => themeButton.classList.contains(iconTheme) ? 'bx-moon' : 'bx-sun'

// validate the previously chosed topic by user
if(selectedTheme){
    document.body.classList[selectedTheme ==='dark' ? 'add' : 'remove'](darktheme)
    themeButton.classList[selectedIcon === 'bx-moon' ? 'add': 'remove'](iconTheme )
}
// active-deactive  the theme manually with the button 
themeButton.addEventListener('click',()=>{
    document.body.classList.toggle(darktheme)
    themeButton.body.classList.toggle(iconTheme)

    localStorage.setitem('selected-theme', getCurrentTheme())
    localStorage.setItem('selected-icon', getCurrentIcon())
})

// scroll reveal animation
const sr= ScrollReveal({
    origin:'top',
    distance:'30px',
    duration: 2000,
    reset: true
});

sr.reveal(`.home__data , .home__img , .about__data, .about__img, .services__content, .menu__content, .app__data, .app__img`, {
    interval: 200
})